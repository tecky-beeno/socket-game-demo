import express from 'express'
import socketIO from 'socket.io'
import http from 'http'
import { setIO } from './socketio'
import { createGameRoute } from './game';

let app = express()
let server = new http.Server(app)
let io = new socketIO.Server(server)
setIO(io)

app.use(express.static('public'))
app.use(createGameRoute())

let PORT = 8100
server.listen(PORT, () => {
  console.log(`http://localhost:${PORT}`)
})
