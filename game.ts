import { Socket } from 'socket.io'
import { io } from './socketio'
import express from 'express'
import debug from 'debug'
import { getUser, User } from './user'
let log = debug('app:game.ts')
// log.enabled = true

let rooms: Room[] = []

class Room {
  id = rooms.length + 1
  channel = 'room:' + this.id
  counter = 0
  members: User[] = []

  constructor() {
    rooms.push(this)
  }

  toJSON() {
    return {
      id: this.id,
      counter: this.counter,
      members: this.members.map(socket => socket.id),
    }
  }

  emit(event: string, data: any) {
    io.to(this.channel).emit(event, data)
  }

  broadcastUpdate() {
    this.emit('room-update', this)
  }

  join(user: User) {
    if (this.members.includes(user) || this.members.length >= 4) {
      return
    }
    this.members.push(user)
    user.join(this.channel)
    this.broadcastUpdate()
  }

  bump() {
    this.counter++
    this.broadcastUpdate()
  }
}

export function createGameRoute() {
  io.on('connection', socket => {
    socket.on('login', data => {
      console.log('login:', data)
      if(!data.username || !data.method){
        socket.disconnect()
        return
      }
      socket.on('login', () => {
        socket.disconnect()
      })
      // TODO check username credential with DB
      let user_id = 123
      let user = getUser(user_id)
      socket.on('create-room', () => {
        let room: Room = new Room()
        room.join(user)
        io.emit('new-room', room)
      })
      socket.on('join-room', id => {
        let idx = id - 1
        let room = rooms[idx]
        if (!room) {
          return
        }
        room.join(user)
      })
    })
    log('connected:', socket.id)
    rooms.forEach(room => socket.emit('new-room', room))
    socket.on('bump-room', id => {
      let idx = id - 1
      let room = rooms[idx]
      if (!room) {
        return
      }
      room.bump()
    })
  })

  let gameRoute = express.Router()
  return gameRoute
}
