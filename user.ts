import { Socket } from 'socket.io'

export class User {
  sockets = new Set<Socket>()
  joinedChannel: string | null = null

  constructor(public id: number) {}

  attachSocket(socket: Socket) {
    this.sockets.add(socket)
    if (this.joinedChannel) {
      socket.join(this.joinedChannel)
    }
    socket.on('disconnect', () => {
      this.sockets.delete(socket)
    })
  }

  emit(event: string, data: any) {
    this.sockets.forEach(socket => socket.emit(event, data))
  }

  join(channel: string) {
    this.leaveChannel()
    this.joinedChannel = channel
    this.sockets.forEach(socket => socket.join(channel))
  }

  leaveChannel() {
    const channel = this.joinedChannel
    if (channel) {
      this.sockets.forEach(socket => {
        socket.leave(channel)
      })
    }
  }
}

let users: User[]

export function getUser(id: number) {
  let user = users[id]
  if (!user) {
    user = new User(id)
    users[id] = user
  }
  return user
}
