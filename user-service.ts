let db: any

// localStorage.setItem('username', input.value)

// localStorage.getItem('username')
// localStorage.getItem('login_method')
// localStorage.getItem('password')
// localStorage.getItem('google_access_token')

export function getUserByPassword(username: string, password: string) {
  db.query(`select id from users where username = $1 and password = $2`, [
    username,
    password,
  ])
}
export function getUserByGoogle(username: string, accessToken: string) {
  db.query(
    `select id from users where username = $1 and google_access_token = $2`,
    [username, accessToken],
  )
}

let data
if (data.login_method === 'password') {
  getUserByPassword(data.username, data.password)
} else {
  getUserByGoogle(data.username, data.accessToken)
}
